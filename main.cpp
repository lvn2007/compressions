#include <string>
#include <iostream>
#include <cstring>
#include <random>
#include "include/shoco.h"
#include "Data.pb.h"

#define BUFFER_SIZE 102400

// Global placeholder
unsigned long int originalSize;
unsigned long int compressedSize;
unsigned long int decompressedSize;

double compressionRatio(int original, int compressed) {
    return ((double)(original - compressed) / (double)original);
}

void generateInputData(Data::rowData *output, std::string dataType, int numElements) {
    // Seed the random number generator
    std::random_device rd;              //Get a random seed from the OS entropy device, or whatever
    std::mt19937_64 eng(rd());       //Use the 64-bit Mersenne Twister 19937 generator and seed it with entropy.

    if (dataType == "string") {
        // Codex
        const std::string CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        // Distribution
        std::uniform_int_distribution<>distribution(0, CHARACTERS.size() - 1);
        std::string tmpStr;

        // Make data
        for (int i = 0; i < numElements; i++) {
            tmpStr = CHARACTERS[distribution(eng)];
            Data::fData *field = output->add_fielddata();
            field->set_stringdata(tmpStr);
        }
    }
    else if (dataType == "uint32") {
        // Distribution
        std::uniform_int_distribution<unsigned int> distr;

        // Make data
        for (unsigned int i = 0; i < numElements; i++) {
            Data::fData *field = output->add_fielddata();
            field->set_uint32data(distr(eng));
        }
    }
    else if (dataType == "uint64") {
        // Distribution
        std::uniform_int_distribution<unsigned long int> distr;

        // Make data
        for (unsigned long int i = 0; i < numElements; i++) {
            Data::fData *field = output->add_fielddata();
            field->set_uint64data(distr(eng));
        }
    }
}

int main() {
    // OPTIONS
    std::string compressOptions = "shoco";
    bool printDecoded = false;

    Data::rowData inputData = Data::rowData();
    generateInputData(&inputData, "uint64", 10);

    // Decompressed data
    std::string originalData = inputData.SerializeAsString();
    originalSize = originalData.length();
    Data::rowData decompressedData = Data::rowData();

    // ---- HUFFMAN
    if (compressOptions == "huffman") {

    }
    else if (compressOptions == "shoco") {
        // Compressed data
        char shocoCompressedData[BUFFER_SIZE] = { 0 };

        // Encode
        size_t compressLength = shoco_compress(originalData.data(), 0, shocoCompressedData, BUFFER_SIZE);
        compressedSize = strlen(shocoCompressedData);

        // Decode
        char shocoDecompressedData[BUFFER_SIZE] = { 0 };
        size_t decompressLength = shoco_decompress(shocoCompressedData, compressLength, shocoDecompressedData, BUFFER_SIZE);
        decompressedSize = strlen(shocoDecompressedData);

        // Parsed serialized back to Protobuf obj
        decompressedData.ParseFromArray(shocoDecompressedData, strlen(shocoDecompressedData));
    }

    if (printDecoded) {
        std::cout << "----------------------------------------" << std::endl;
        std::cout << "[DEBUG] Original data: " << std::endl;
        inputData.PrintDebugString();

        std::cout << "\n[DEBUG] Decompressed data: " << std::endl;
        decompressedData.PrintDebugString();
        std::cout << "----------------------------------------" << std::endl;
    }

    // Validating correctness
    if (inputData.SerializeAsString() == decompressedData.SerializeAsString()) {
        std::cout << "[VALIDATION] Original data matches decompressed data" << std::endl;

        // Data info
        std::cout << "[INFO] Original size: " << originalSize << " bytes" << std::endl;
        std::cout << "[INFO] Compressed size: " << compressedSize << " bytes" << std::endl;
        std::cout << "[INFO] Decompressed size: " << decompressedSize << " bytes" << std::endl;
        std::cout << "[INFO] Compression ratio: " << compressionRatio(originalSize, compressedSize) << std::endl;
    }
    else {
        std::cout << "[VALIDATION] Original data DOES NOT match decompressed data" << std::endl;
    }
    return 0;
}
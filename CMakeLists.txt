cmake_minimum_required(VERSION 3.25)
project(compressions)

set(CMAKE_CXX_STANDARD 17)
message(STATUS "Using C++ Standard: ${CMAKE_CXX_STANDARD}")

find_package(Protobuf REQUIRED)

# Protobuf-compiler
set(_PROTOBUF_PROTOC $<TARGET_FILE:protobuf::protoc>)
message(STATUS "Using Protobuf ${Protobuf_VERSION}")

# Proto file
get_filename_component(data_proto "Data.proto" ABSOLUTE)
get_filename_component(data_proto_path "${data_proto}" PATH)

add_custom_command(
        OUTPUT "${data_grpc_srcs}" "${data_grpc_hdrs}"
        COMMAND ${_PROTOBUF_PROTOC}
        ARGS --grpc_out "${CMAKE_CURRENT_BINARY_DIR}"
        --cpp_out "${CMAKE_CURRENT_BINARY_DIR}"
        -I "${data_proto_path}"
        --plugin=protoc-gen-grpc="${_GRPC_CPP_PLUGIN_EXECUTABLE}"
        "${data_proto}"
        DEPENDS "${data_proto}")

include_directories(.)
include_directories(include)
include_directories(${Protobuf_INCLUDE_DIRS})
protobuf_generate_cpp(PROTO_SRCS PROTO_HDRS Data.proto)

# Include generated *.pb.h files
include_directories("${CMAKE_CURRENT_BINARY_DIR}")

add_executable(compressions main.cpp ${PROTO_SRCS} ${PROTO_HDRS} Compressions/huffman.cpp include/huffman.h Compressions/shoco.cpp include/shoco.h include/shoco_model.h)
add_compile_options(-O3)
target_link_libraries(compressions PUBLIC ${Protobuf_LIBRARIES})